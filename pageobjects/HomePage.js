module.exports = {

	elements: {

		newAccountBtn : {
			selector : '.create-account-button'
		},

		menuTrigger : {
        	selector : '.user-menu-trigger'
        },

        newReqTab : {
        	selector : '.menu-container > li:nth-of-type(1)'
        },

        newReqHeader : {
        	selector : '.new-organizer-req-table-container >.new-org-text-header'
        },

        activeAccTab : {
        	selector : '.menu-container > li:nth-of-type(2)'
        },

        inactiveAccTab : {
        	selector : '.menu-container > li:nth-of-type(3)'
        },

        activeAccountsTble : {
        	selector : '.row.active-table-container'
        },

        plusOrganizer : {
        	selector : '.add-botton-modal > * .glyphicon.glyphicon-plus'
        },

        approveBtn : {
        	selector : '.yellow-btn-square.btn.btn-primary'
        },

        successMsg : {
        	selector : '.subtitle-modal'
        },

        acceptBtn : {
        	selector : '.yellow-btn-square.btn.btn-primary'
        }
	},

	commands: [{

		clickNewAccount() {
			this
			.waitForElementVisible("@newAccountBtn", 2000)
            .click("@newAccountBtn")
			return this.api;
		},

		selectSettings() {
			this
			.waitForElementVisible("@menuTrigger", 2000)
			.click("@menuTrigger",()=>{
                 this.api.useXpath().waitForElementPresent("//div[text()='settings']", 2000);
                 this.click("xpath", "//div[text()='settings']");
              })
			return this.api;
		},

		selectLogout() {
			this
			.waitForElementVisible("@menuTrigger", 2000)
			.click("@menuTrigger",()=>{
                 this.api.useXpath().waitForElementPresent("//div[text()='logout']", 2000);
                 this.click("xpath", "//div[text()='logout']");
              })
			return this.api;
		},

		selectNewRequestsTab() {
			this
			.api.useXpath().waitForElementVisible("//div[@class='menu-container']/li[text()='NEW REQUESTS (']", 2000)
			.click("xpath", "//div[@class='menu-container']/li[text()='NEW REQUESTS (']")
			return this.api;
		},

		selectInactiveAccountsTab() {
			this
			.waitForElementVisible("@inactiveAccTab", 2000)
            .click("@inactiveAccTab")
			return this.api;
		},

		selectActiveAccountTab() {
			this
			.waitForElementVisible("@activeAccTab", 2000)
            .click("@activeAccTab")
			return this.api;
		},

		selectAccount(value) {
			this
			.waitForElementVisible("@activeAccountsTble", 2000)
            .api.useXpath().waitForElementPresent("//div[text()='" + value + "']", 2000)
            .click("xpath", "//div[text()='" + value + "']")
			return this.api;
		},

		selectInactiveAccount(value) {
			this
            .api.useXpath().waitForElementPresent("//div[text()='" + value + "']", 2000)
			return this.api;
		},

		clickRequestAction(value){
			this
			.api.useXpath().waitForElementPresent("//div[@class='col-sm-5']/div[text()='" + value + "']", 2000, "Request found")
			.click("xpath", "//div[@class='col-sm-5']/div[text()='" + value + "']",()=>{
                this.click("xpath", "//div[@class='col-sm-5']/div[text()='" + value + "']/following-sibling::div")
             })
            return this.api;
		},

		clickApprove(){
			this
			.waitForElementVisible("@approveBtn", 2000)
			.click("@approveBtn")
			return this.api;
		},

		clickAccept(msg){
			this
			.waitForElementVisible("@successMsg", 2000)
			this.click("@acceptBtn")
			return this.api;
		}


	}]

};