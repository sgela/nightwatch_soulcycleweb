module.exports = {

	elements: {

		editBtn : {
        	selector : '.edit-botton.btn.btn-default'
        },

        minimumBikes : {
        	selector : 'select[name="minBikes"]'
        },

        maximumBikes : {
        	selector : 'select[name="maxBikes"]'
        },

        plusPriceMatrix : {
        	selector : '.pricing-matrix-container > .glyphicon.glyphicon-plus'
        },

        plusAdmin : {
        	selector : '.section >* .glyphicon.glyphicon-plus'
        },

        saveBtn : {
        	selector : 'button[type="submit"]'
        },

        successMsg : {
        	selector : '.subtitle-modal'
        },

        acceptBtn : {
        	selector : '.button-container > .yellow-btn-square.btn.btn-primary'
        }

	},

	commands: [{

		clickEdit(){
			this
			.waitForElementVisible("@editBtn", 2000)
			.click("@editBtn")
			return this.api;
		},

		selectMinimumBikes(value){
			this
			 	.waitForElementVisible("@minimumBikes",2000,()=>{
                 this.click("@minimumBikes")
                 this.waitForElementVisible("select[name='minBikes'] option[value='"+ value + "']")
                 //this.click("select[name='minBikes'] option[value='"+ value + "']")
                 this.click("xpath", "//option[text()='"+ value + "']")
             })
            
			 this.expect.element('@minimumBikes').value.to.equal(value)
			return this.api;
		},

		selectMaximumBikes(value){
			this
			      .waitForElementVisible("@maximumBikes",2000, ()=>{
			 	  this.click("@maximumBikes")
                  this.click("select[name='maxBikes'] option[value='"+ value + "']")
            })
            
			 .expect.element('@maximumBikes').value.to.equal(value)
			return this.api;
		},

		clickAddPricing(){
			this
			.waitForElementVisible("@plusPriceMatrix", 2000)
			.click("@plusPriceMatrix")
			return this.api;
		},

		modifyPricing(value){
			this
			.api.useXpath().waitForElementPresent("//li[@class='row']/div[@class='col-xs-3']/div[text()='" + value + "']", 2000)
			/*.click("xpath", "//div[@class='col-sm-5']/div[text()='" + value + "']",()=>{
                this.click("xpath", "//div[@class='col-sm-5']/div[text()='" + value + "']/following-sibling::div")
             })*/
            return this.api;
		},

		clickAddAdmin(){
			this
			.waitForElementVisible("@plusAdmin", 2000)
			.click("@plusAdmin")
			return this.api;
		},

		clickSaveBtn(){
			this
			.waitForElementVisible("@saveBtn", 2000)
			.click("@saveBtn")
			return this.api;
		},

		clickAccept(msg){
			this
			.waitForElementVisible("@successMsg", 2000)
			this.click("@acceptBtn")
			return this.api;
		}


	}]

};