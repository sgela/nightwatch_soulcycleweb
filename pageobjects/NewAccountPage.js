module.exports = {

	elements : {

		headerTitle : {
			selector : 'header-title'
		},

		activateAccBtn : {
			selector : '.yellow-btn-activate.btn.btn-primary'
		},

		acceptCreatedMsg : {
			selector : '.yellow-btn-square.btn.btn-primary'
		},

        // ---- Company Info ---- //

		companyName : {
			selector : 'input[name="companyName"]'
		},

		bussinessUnit : {
			selector : 'input[name="accountBusinessUnit"]'
		},

		companyAddress : {
			selector : 'input[name="companyAddress"]'
		},

		city : {
			selector : 'input[name="city"]'
		},

		state : {
			selector : 'select[name="state"]'
		},

		companyZipCode : {
			selector : 'input[name="zipCode"]'
		},

		// ---- Contact Info ---- //

		firstName : {
			selector : 'input[name="firstName"]'
		},

		lastName : {
			selector : 'input[name="lastName"]'
		},

		title : {
			selector : 'input[name="title"]'
		},

		email : {
			selector : 'input[name="email"]'
		},

		phoneNumber : {
			selector : '.react-tel-input > input[type="tel"]'
		},

		// ---- Price Discount ---- //

		applyCompanyDiscount : {
			selector : 'input[name="discountPrice"]'
		},

		// ---- Region of Interest ---- //

		regionInterest : {
			selector : 'select[name="regionSelected"]'
		},

		studioInterest : {
			selector : 'select[name="studioSelected"]'
		},

		// ---- Credit Card Info ---- //

		cardNickName : {
			selector : 'input[name="cardNickName"]'
		},

		cardHolderName : {
			selector : 'input[name="cardHolderName"]'
		},

		creditCardNumber : {
			selector : 'input[name="creditCardNumber"]'
		},

		expirationMonth : {
			selector : 'select[name="exp_month"]'
		},

		expirationYear : {
			selector : 'select[name="exp_year"]'
		},

		cardCvv : {
			selector : '.col-xs-6.cvv > * input'
		},

		cardZip : {
			selector : '.col-xs-6.zip-code > * input'
		}


	},

	commands : [{

		verifyTitle(){
			this
			.waitForElementVisible("@headerTitle")
			.expect.element('@headerTitle').value.to.equal("Create New Account")
			return this.api;
		},

		clickActivateAcc(){
			this
			.waitForElementVisible("@activateAccBtn")
			.click("@activateAccBtn")
			return this.api;
		},

		clickAcceptBtn(){
			this
			.waitForElementVisible("@acceptCreatedMsg")
			.click("@acceptCreatedMsg")
			return this.api;
		},

		// ---- Company Info ---- //

		companyNameInput(value){
			this
			.waitForElementVisible("@companyName")
			.setValue("@companyName", value)
			.expect.element('@companyName').value.to.equal(value)
			return this.api;
		},

		bussinessUnitInput(value){
			this
			.waitForElementVisible("@bussinessUnit")
			.setValue("@bussinessUnit", value)
			.expect.element('@bussinessUnit').value.to.equal(value)
			return this.api;
		},

		companyAddressInput(value){
			this
			.waitForElementVisible("@companyAddress")
			.setValue("@companyAddress", value)
			.expect.element('@companyAddress').value.to.equal(value)
			return this.api;
		},

		companyCityInput(value){
			this
			.waitForElementVisible("@city")
			.setValue("@city", value)
			.expect.element('@city').value.to.equal(value)
			return this.api;
		},

		selectState(value){
			this
			      .waitForElementVisible("@state",2000, ()=>{
                  this.click("select[name='state'] option[value='"+ value + "']")
            })
            .expect.element('@state').value.to.equal(value)
			return this.api;
		},

		companyZipCodeInput(value){
			this
			.waitForElementVisible("@companyZipCode")
			.setValue("@companyZipCode", value)
			.expect.element('@companyZipCode').value.to.equal(value)
			return this.api;
		},

		// ---- Contact Info ---- //

		firstNameInput(value){
			this
			.waitForElementVisible("@firstName")
			.setValue("@firstName", value)
			.expect.element('@firstName').value.to.equal(value)
			return this.api;
		},

		lastNameInput(value){
			this
			.waitForElementVisible("@lastName")
			.setValue("@lastName", value)
			.expect.element('@lastName').value.to.equal(value)
			return this.api;
		},

		titleInput(value){
			this
			.waitForElementVisible("@title")
			.setValue("@title", value)
			.expect.element('@title').value.to.equal(value)
			return this.api;
		},

		emailInput(value){
			this
			.waitForElementVisible("@email")
			.setValue("@email", value)
			.expect.element('@email').value.to.equal(value)
			return this.api;
		},

		phoneNumberInput(value){
			this
			.waitForElementVisible("@phoneNumber")
			.setValue("@phoneNumber", value)
			return this.api;
		},

		// ---- Price Discount ---- //

		applyPriceDiscountInput(value){
			this
			.waitForElementVisible("@applyCompanyDiscount")
			.setValue("@applyCompanyDiscount", value)
			.expect.element('@applyCompanyDiscount').value.to.equal(value)
			return this.api;
		},

		// ---- Region of Interest ---- //

		selectRegion(value){
			this
			      .waitForElementVisible("@regionInterest",2000, ()=>{
                  this.click("select[name='regionSelected'] option[value='"+ value + "']")
            })
            .expect.element('@regionInterest').value.to.equal(value)
			return this.api;
		},

		selectStudio(value){
			this
			      .waitForElementVisible("@studioInterest",2000, ()=>{
			      	this.waitForElementVisible("select[name='studioSelected'] option[value='"+ value + "']")
                  .click("select[name='studioSelected'] option[value='"+ value + "']")
            })
            .expect.element('@studioInterest').value.to.equal(value)
			return this.api;
		},

		// ---- Credit Card Info ---- //

		cardNickNameInput(value){
			this
			.waitForElementVisible("@cardNickName")
			.setValue("@cardNickName", value)
			.expect.element('@cardNickName').value.to.equal(value)
			return this.api;
		},

		cardHolderNameInput(value){
			this
			.waitForElementVisible("@cardHolderName")
			.setValue("@cardHolderName", value)
			.expect.element('@cardHolderName').value.to.equal(value)
			return this.api;
		},

		creditCardNumberInput(value){
			this
			.waitForElementVisible("@creditCardNumber")
			.setValue("@creditCardNumber", value)
			//.expect.element('@creditCardNumber').value.to.equal(value)
			return this.api;
		},

		selectExpirationMonth(value){
			this
			      .waitForElementVisible("@expirationMonth",2000, ()=>{
                  this.click("select[name='exp_month'] option[value='"+ value + "']")
            })
            .expect.element('@expirationMonth').value.to.equal(value)
			return this.api;
		},

		selectExpirationYear(value){
			this
			      .waitForElementVisible("@expirationYear",2000, ()=>{
                  this.click("select[name='exp_year'] option[value='"+ value + "']")
            })
            .expect.element('@expirationYear').value.to.equal(value)
			return this.api;
		},

		cardCvvInput(value){
			this
			.waitForElementVisible("@cardCvv")
			.setValue("@cardCvv", value)
			.expect.element('@cardCvv').value.to.equal(value)
			return this.api;
		},

		cardZipInput(value){
			this
			.waitForElementVisible("@cardZip")
//setValue("@cardZip", value)
			.expect.element('@cardZip').value.to.equal(value)
			return this.api;
		}


	}]
};