module.exports = {

	elements: {

		bookClassesBtn : {
			selector : '.create-account-button'
		}


	},

	commands: [{

	    clickBookClasses(){
	    	this
	    	.waitForElementPresent("@bookClassesBtn", 2000)
	    	.click("@bookClassesBtn")
	    	return this.api;
	    }

	}]

};