module.exports  = {

	elements: {

		corporateLogin : {
           selector : '.login-text.btn.btn-primary'
           //selector : '#root > div > div:nth-of-type(2) > .header-wrapper > * .login-text.btn.btn-primary'
        },

        username : {
        	selector : '.form-control[name="username"]'
        },

        password : {
        	selector : '.form-control[name="password"]'
        },

        loginBtn : {
        	selector : '.login-button.btn.btn-primary'
        },

        menuTrigger : {
        	selector : '.user-menu-trigger'
        },

	},

	commands: [{

		clickCorporateLogin(){
			this
			.waitForElementPresent("@corporateLogin", 2000)
			.click("@corporateLogin")
			return this.api;
		},

		userInput(email){
			this
			.waitForElementVisible("@username", 2000)
			.click("@username")
			.setValue("@username", email)
			.expect.element('@username').value.to.equal(email)
			return this.api;
		},

		pwdInput(pwd){
			this
			.waitForElementVisible("@password", 2000)
			.click("@password")
			.setValue("@password", pwd)
			return this.api;
		},

		clickLogin(){
			this
			.waitForElementVisible("@loginBtn", 2000) 
			.click("@loginBtn")
			return this.api;
		},

		verifyUser(usr){
			this
			.waitForElementVisible("@menuTrigger")
			.expect.element("@menuTrigger").text.to.contain(usr)
			return this.api;
		},

		cgbLogin(email, pwd){
			this.userInput(email);
			this.pwdInput(pwd);
			this.clickLogin();
		}

	}]

};