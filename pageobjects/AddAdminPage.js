module.exports = {

	elements: {

        firstName : {
        	selector : '.form-control[name="firstName"]'
        },

        lastName : {
        	selector : '.form-control[name="lastName"]'
        },

        email : {
        	selector : '.form-control[name="organizerEmail"]'
        },

        submitBtn : {
        	selector : '.add-organizer-botton-container > button[type="submit"]'
        },

        successMsg : {
        	selector : '.subtitle-modal'
        },

        acceptBtn : {
        	selector : '.confirm-modal-container > * .yellow-btn-square.btn.btn-primary'
        },





	},

	commands: [{

		inputFname(fname){
			this
			.waitForElementVisible("@firstName", 2000)
			.setValue("@firstName", fname)
			.expect.element('@firstName').value.to.equal(fname)
			return this.api;
		},

		inputLname(lname){
			this
			.waitForElementVisible("@lastName", 2000)
			.setValue("@lastName", lname)
			.expect.element('@lastName').value.to.equal(lname)
			return this.api;
		},
        
        inputEmail(email){
			this
			.waitForElementVisible("@email", 2000)
			.setValue("@email", email)
			.expect.element('@email').value.to.equal(email)
			return this.api;
		},

		clickSubmit(){
			this
			.waitForElementVisible("@submitBtn", 2000)
			.click("@submitBtn")
			.waitForElementVisible("@successMsg", 4000)
			return this.api;
		},

		clickAccept(){
			this
			.waitForElementVisible("@acceptBtn", 2000)
			.click("@acceptBtn")
			return this.api;
		}

	}]

};