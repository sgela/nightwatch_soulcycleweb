module.exports = {

	elements: {

		deactivateAccBtn : {
			selector : '.gray-btn-desactivate.btn.btn-primary'
		},

        plusOrganizer : {
        	selector : '.add-botton-modal > * .glyphicon.glyphicon-plus'
        },

        manageAccountBtn : {
        	selector : '.yellow-btn-manage.btn.btn-primary'
        },

        addOrgTitle : {
        	selector : '.title-with-top-padding'
        },

        businessUnit : {
        	selector : '.form-control[name="businessUnit"]'
        },

        firstName : {
        	selector : '.form-control[name="firstName"]'
        },

        lastName : {
        	selector : '.form-control[name="lastName"]'
        },

        email : {
        	selector : '.form-control[name="organizerEmail"]'
        },

        phone : {
        	selector : '.react-tel-input > input[type="tel"]'
        },

        addOrgBtn : {
        	selector : '.add-organizer-btn.btn.btn-primary'
        },

        saveAccInfo : {
        	selector : '.yellow-btn-save.btn.btn-primary'
        },

        acceptBtn : {
        	selector : '.yellow-btn-square.btn-accept.btn.btn-primary'
        },

        // --- Company Info --- //

        companyName : {
			selector : 'input[name="companyName"]'
		},

		bussinessUnit : {
			selector : 'input[name="accountBusinessUnit"]'
		},

		companyAddress : {
			selector : 'input[name="companyAddress"]'
		},

		city : {
			selector : 'input[name="city"]'
		},

		state : {
			selector : 'select[name="state"]'
		},

		companyZipCode : {
			selector : 'input[name="zipCode"]'
		}

	},

	commands: [{

		clickDeactivateAcc(){
			this
			.waitForElementVisible("@deactivateAccBtn", 2000)
			.click("@deactivateAccBtn")
			return this.api;
		},
		
		clickManageAcc(){
			this
			.waitForElementVisible("@manageAccountBtn", 2000)
			.click("@manageAccountBtn")
			return this.api;
		},
		
		addOrganizer(){
			this
			.waitForElementVisible("@plusOrganizer", 2000)
			.getLocationInView("@plusOrganizer")
			.click("@plusOrganizer")
			return this.api;
		},

		verifyTitle(){
			this
			.waitForElementVisible("@addOrgTitle")
			.expect.element("@addOrgTitle").text.to.contain("Add Organizer")
			return this.api;
		},

		inputBusinessUnit(bsunit){
			this
			.waitForElementVisible("@businessUnit", 2000)
			.setValue("@businessUnit", bsunit)
			.expect.element('@businessUnit').value.to.equal(bsunit)
			return this.api;
		},

		inputFname(fname){
			this
			.waitForElementVisible("@firstName", 2000)
			.setValue("@firstName", fname)
			.expect.element('@firstName').value.to.equal(fname)
			return this.api;
		},

		inputLname(lname){
			this
			.waitForElementVisible("@lastName", 2000)
			.setValue("@lastName", lname)
			.expect.element('@lastName').value.to.equal(lname)
			return this.api;
		},
        
        inputEmail(email){
			this
			.waitForElementVisible("@email", 2000)
			.setValue("@email", email)
			.expect.element('@email').value.to.equal(email)
			return this.api;
		},

		inputPhone(phone){
			this
			.waitForElementVisible("@phone", 2000)
			.setValue("@phone", phone)
			return this.api;
		},

		clickAddOrg(){
			this
			.waitForElementVisible("@addOrgBtn", 2000)
			.click("@addOrgBtn")
			return this.api;
		},

		clickAccept(){
			this
			.waitForElementVisible("@acceptBtn", 2000)
			.click("@acceptBtn")
			return this.api;
		},

		clickSaveAccInfoBtn(){
			this
			.waitForElementVisible("@saveAccInfo", 2000)
			.click("@saveAccInfo")
			return this.api;
		},

		// --- Company Info --- //

		companyNameInput(value){
			this.waitForElementVisible("@companyName");
			this.clearValue("@companyName");
			this.setValue("@companyName", value)
			.expect.element('@companyName').value.to.equal(value)
			return this.api;
		},

		bussinessUnitInput(value){
			this.waitForElementVisible("@bussinessUnit");
			this.clearValue("@bussinessUnit");
			this.setValue("@bussinessUnit", value)
			.expect.element('@bussinessUnit').value.to.equal(value)
			return this.api;
		},

		companyAddressInput(value){
			this.waitForElementVisible("@companyAddress");
			this.clearValue("@companyAddress");
			this.setValue("@companyAddress", value)
			.expect.element('@companyAddress').value.to.equal(value)
			return this.api;
		},

		companyCityInput(value){
			this.waitForElementVisible("@city");
			this.clearValue("@city");
			this.setValue("@city", value)
			.expect.element('@city').value.to.equal(value)
			return this.api;
		},

		selectState(value){
			this
			      .waitForElementVisible("@state",2000, ()=>{
                  this.click("select[name='state'] option[value='"+ value + "']")
            })
            .expect.element('@state').value.to.equal(value)
			return this.api;
		},

		companyZipCodeInput(value){
			this.waitForElementVisible("@companyZipCode");
			this.clearValue("@companyZipCode");
			this.setValue("@companyZipCode", value)
			.expect.element('@companyZipCode').value.to.equal(value)
			return this.api;
		},
	}]

};