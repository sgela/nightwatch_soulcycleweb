module.exports = {

	elements: {

		selectRegion : {
			selector : '.location > * .filter-toggle'
		},

		selectStudio : {
			selector : '.studio > * .filter-toggle'
		},

		selectDate : {
			selector : '.date > * .filter-toggle'
		},

		calendarDate : {
			selector : '.DayPicker-Caption > div'
		},

		backCalendarBtn : {
			selector : '.DayPicker-NavButton.DayPicker-NavButton--prev'
		},

		classSelection : {
			selector : '.page > .class'
		},

		classReserve : {
			selector : '.class> .class-link > .class-reserve > .reserve-bagde'
		},

		bikesMap : {
			selector : '.map-bike-component'
		},

		checkoutBtn : {
			selector : '.btn-checkout'
		},

		purchaseBtn : {
			selector : '.yellow-btn-square.btn.btn-lg.btn-primary.btn-block'
		},

		acceptBtn : {
			selector : '.yellow-btn-square.btn.btn-primary'
		}


	},

	commands: [{

	    selectRegion(value){
	    	this
	    	.waitForElementPresent("@selectRegion", 2000)
	    	.click("@selectRegion", ()=>{
	    		this.api.useXpath().waitForElementPresent("//button[text()='" + value + "']", 2000);
	    		  this.click("xpath", "//div[@class='filter-content']/button[text()='" + value + "']");
            })
            .expect.element("@selectRegion").text.to.contain(value)
	    	return this.api;
	    },

	    selectStudio(value, value2, value3){
	    	this
	    	.waitForElementPresent("@selectStudio", 3000);
	    	this.api.pause(800); // Until CGB is stable, this is needed to execute the click on Studio selection.
	    	this.click("@selectStudio", ()=>{
	    		this.api.useXpath().waitForElementPresent("//button[text()='" + value + "']", 8000)
	    		  .click("xpath", "//button[text()='" + value + "']")
	    		  .click("xpath", "//button[text()='" + value2 + "']")
	    		  .click("xpath", "//button[text()='" + value3 + "']");
            })
	    	return this.api;
	    },

	    selectDate(value){
	    	this
	    	.waitForElementPresent("@selectDate", 2000)
	    	.click("@selectDate", ()=>{
	    		  this.click("div[aria-selected='true']");
            })
	    	return this.api;
	    },

	    selectClass(){
	    	this
	    	.waitForElementPresent("@classSelection", 5000)
	    	.click("@classSelection")
	    	.click("@classReserve")
	    	return this.api;
	    },

	    selectBikes(value){
	    	this
	    	.waitForElementPresent("@bikesMap", 9000);
	    	this.api.useXpath().waitForElementPresent("//span[text()='" + value + "']", 3000)
	    	.click("xpath", "//span[text()='" + value + "']")
	    	return this.api;
	    },

	    clickCheckoutBtn(){
	    	this
	    	.waitForElementPresent("@checkoutBtn", 3000)
	    	.click("@checkoutBtn")
	    	return this.api;
	    },

	    clickPurchaseBtn(){
	    	this
	    	.waitForElementPresent("@purchaseBtn", 4000)
	    	.click("@purchaseBtn")
	    	return this.api;
	    },

	    acceptPurchase(){
	    	this
	    	.api.useXpath().waitForElementPresent("//button[text()='ACCEPT']", 3000)
	    	.click("xpath", "//button[text()='ACCEPT']")
	    	return this.api;
	    }



	}]

};