var config = {
  selenium : {  
    host: process.env.CGB_AUTOMATION_SELENIUM_HOST || '127.0.0.1',
    port: process.env.CGB_AUTOMATION_SELENIUM_PORT || '4444',
  },

  globals : {
    cgb_url: process.env.CGB_URL || 'http://agb-qa.soul-podium.net/',
    cpt_usr: process.env.CGB_USER,
    cpt_pwd: process.env.CGB_PASSWORD,
    org_usr: process.env.ORG_USER,
    org_pwd: process.env.ORG_PASSWORD,
  },
 }

module.exports = config;
