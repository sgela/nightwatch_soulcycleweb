module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['org'],
  'Add Organizer Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var activeAcc = browser.page.ActiveAccountsPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
	home.selectActiveAccountTab();
	home.selectAccount("Bethesda"); // Your account name
	activeAcc.addOrganizer();
	activeAcc.verifyTitle();
	activeAcc.inputBusinessUnit("22582");
	activeAcc.inputFname("Jon");
	activeAcc.inputLname("Stark");
	activeAcc.inputEmail("kingofthenorth@email.com");
	activeAcc.inputPhone("2624445555");
	activeAcc.clickAddOrg();
	browser.expect.element(".subtitle-modal").text.to.contain("Organizer succesfully created.");
	activeAcc.clickAccept();

 },

};