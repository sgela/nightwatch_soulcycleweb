module.exports = {

   before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

  '@tags': ['cpt'],
  'CPT Login Test' : function (browser) {
   var login = browser.page.LoginPage();
	 browser.assert.title("SoulCycle Corporate Portal");
   login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	 login.verifyUser("Tony"); // This is the Name of the user you have log into
 },

};