module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Deactivate Account Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var activeAcc = browser.page.ActiveAccountsPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
	home.selectActiveAccountTab();
	home.selectAccount("Bethesda"); // Your account name
	activeAcc.clickDeactivateAcc();
  home.selectInactiveAccountsTab();
  home.selectInactiveAccount("Bethesda");
 },

};