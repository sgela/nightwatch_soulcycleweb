module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Change Booking Preferences Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var settings = browser.page.SettingsPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
	home.selectSettings();
	browser.assert.containsText(".col-xs-10.top-title", "Settings");
	settings.clickEdit();
	settings.selectMinimumBikes("7"); // Select your min bikes
	settings.selectMaximumBikes("34"); // Select your max bikes
	settings.clickSaveBtn();
	browser.assert.containsText(".subtitle-modal", "Information successfully saved");
	settings.clickAccept();
 },

};