module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Add Account Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var acc = browser.page.NewAccountPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
	home.clickNewAccount();
	acc.companyNameInput("Bethesda");
	acc.bussinessUnitInput("11380");
	acc.companyAddressInput("462 Rockwell Ave.");
	acc.companyCityInput("Orange Park");
	acc.selectState("FL"); // State Value, for e.g CA, NJ, NY
	acc.companyZipCodeInput("32065");
	acc.firstNameInput("Bruno");
	acc.lastNameInput("Diaz");
	acc.emailInput("find.joker"+Math.random()+"@email.com");
	acc.phoneNumberInput("2625554444");
	acc.applyPriceDiscountInput("0")
	acc.cardNickNameInput("batman");
	acc.cardHolderNameInput("Bruno Diaz");
	acc.creditCardNumberInput("4007000000027");
	acc.selectExpirationMonth("11");
	acc.selectExpirationYear("23"); // These are Year values for e.g 20, 21 ,22 
	acc.cardCvvInput("900");
	acc.cardZipInput("32065");
	acc.clickActivateAcc();
	browser.expect.element(".subtitle-modal").text.to.contain("Account created").before(3000);
	acc.clickAcceptBtn();

 },

};