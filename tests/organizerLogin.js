module.exports = {

   before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

  '@tags': ['org'],
  'Organizer Login Test' : function (browser) {
   var login = browser.page.LoginPage();
	 browser.assert.title("SoulCycle Corporate Portal");
   login.cgbLogin(browser.globals.ORG_USR, browser.globals.ORG_PWD);
	 login.verifyUser("SweatWorks Test"); // This is the Name of the user you have log into
 },

};