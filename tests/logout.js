module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Logout Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var settings = browser.page.SettingsPage();
	  browser.assert.title("SoulCycle Corporate Portal");
	  login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	  login.verifyUser("Tony"); // This is the Name of the user you have log into
	  home.selectLogout();
    browser.assert.elementPresent(".login-button.btn.btn-primary");
},
};