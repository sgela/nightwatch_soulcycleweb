module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Modify Account Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var activeAcc = browser.page.ActiveAccountsPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
  home.selectActiveAccountTab();
  home.selectAccount("Bethesda"); // Your account name
  activeAcc.companyNameInput("Bethesda");
  activeAcc.bussinessUnitInput("11380");
  activeAcc.companyAddressInput("462 Rockwell Ave.");
  activeAcc.companyCityInput("Orange Park");
  activeAcc.selectState("FL"); // State Value, for e.g CA, NJ, NY
  activeAcc.companyZipCodeInput("32065");
  activeAcc.clickSaveAccInfoBtn();
  browser.assert.containsText(".subtitle-modal", "Are you sure you want to update the information?");
  activeAcc.clickAccept();
  

 },

};