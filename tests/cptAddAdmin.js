module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Add Admin Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var settings = browser.page.SettingsPage();
  	var adm = browser.page.AddAdminPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
	home.selectSettings();
	browser.assert.containsText(".col-xs-10.top-title", "Settings");
	settings.clickEdit();
	settings.clickAddAdmin();
	adm.inputFname("Jon"); // First Name 
	adm.inputLname("Snow2"); // Last Name
	adm.inputEmail("batman"+Math.random()+"@email.com"); // Email
	adm.clickSubmit();
	browser.assert.containsText(".subtitle-modal", "CPT user successfully added");
	adm.clickAccept();
 },

};