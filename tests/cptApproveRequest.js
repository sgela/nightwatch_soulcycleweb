module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['cpt'],
  'Approve Request Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var activeAcc = browser.page.ActiveAccountsPage();
	browser.assert.title("SoulCycle Corporate Portal");
  login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
	home.selectNewRequestsTab();
	home.clickRequestAction("batman@email.com"); // The mail of the organizer you want to approve - unique
  home.clickApprove();
  browser.expect.element(".subtitle-modal").text.to.contain("You have approved the organizer").before(3000);
  home.clickAccept();
 },

};