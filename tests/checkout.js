module.exports = {
 
 before(browser) {
    browser.windowMaximize();
    browser.url(browser.globals.CGB_URL);  
 },
   after(browser){
    browser.end();
 },

 '@tags': ['In progress'],
  'Checkout Test' : function (browser) {
  	var login = browser.page.LoginPage();
  	var home = browser.page.HomePage();
  	var activeAcc = browser.page.ActiveAccountsPage();
    var orgBoard = browser.page.OrgDashboardPage();
    var book = browser.page.BookClassesPage();
	browser.assert.title("SoulCycle Corporate Portal");
	login.cgbLogin(browser.globals.CPT_USR, browser.globals.CPT_PWD);
	login.verifyUser("Tony"); // This is the Name of the user you have log into
  home.selectActiveAccountTab();
  home.selectAccount("Bethesda"); // Your account name
  activeAcc.clickManageAcc();
  browser.expect.element(".cpt-banner.text-center").text.to.contain("Ghost Mode").before(3000);
  orgBoard.clickBookClasses();
  book.selectRegion("Northern California");
  book.selectStudio("Palo Alto");
  book.selectDate();
  book.selectClass();
  book.selectBikes("56");
  book.clickCheckoutBtn();
  book.clickPurchaseBtn();
  browser.expect.element(".subtitle-modal").text.to.contain("Do you confirm the checkout?");
  book.acceptPurchase();
  //browser.expect.element("//button[text()='PRINT RECEIPT']").text.to.contain("PRINT RECEIPT").before(9000);
  
 },

};