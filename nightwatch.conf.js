// we use a nightwatch.conf.js file so we can include comments and helper functions

const LIBPATH = 'lib/';
var config = require('./config.js');

module.exports = {
  "src_folders": [
    "tests"
  ],
  "page_objects_path": "./pageobjects",
  //"output_folder": "", // reports (test outcome) output by nightwatch
  "selenium": { // downloaded by selenium-download module (see readme)
    "start_process": true, // tells nightwatch to start/stop the selenium process
     "server_path": "node_modules/selenium-server/lib/runner/selenium-server-standalone-3.12.0.jar",
    "host": config.selenium.host,
    "port": config.selenium.port, // standard selenium port
    "cli_args": { // chromedriver is downloaded by selenium-download 
       "webdriver.chrome.driver" : "node_modules/chromedriver/lib/chromedriver/chromedriver"
    }
  },

  "test_workers" : {"enabled" : true, "workers" : 11 }, // perform tests in parallel where possible    
  "test_settings": {
    "default": {
       "silent": true,
      "screenshots": {
        "enabled": true, // if you want to keep screenshots
        "path": './screenshots' // save screenshots here
      },
      "globals": {
        "waitForConditionTimeout": 5000, // sometimes internet is slow so wait.
        "CGB_URL" : config.globals.cgb_url, // url for soulcycle corporate group booking
        "CPT_USR" : config.globals.cpt_usr,
        "CPT_PWD" : config.globals.cpt_pwd,
        "ORG_USR" : config.globals.org_usr,
        "ORG_PWD" : config.globals.org_pwd
      },

    // ---- Chrome options ---- //  

    
     "desiredCapabilities" : {
                "browserName": "chrome",
                "javascriptEnabled" : true,
                "chromeOptions" : {
                "args" : ["--no-sandbox"]
            },
                "acceptSslCerts" : true

            }
    
    }
  }
}