# podium-gbp-ui-automation
UI Automation for Corporate Group Bookings


### Usage (Locally)

- Run `$ docker-compose up` in podium-gbp-ui-automation's root folder


### Available configuration variables

| Name | Description | Default Value | Required |
| ---- | ----------- | ------------- | -------- |
| **Application** | | | |
| CGB_AUTOMATION_SELENIUM_HOST | The host of selenium server  | 127.0.0.1 | no |
| CGB_AUTOMATION_SELENIUM_PORT | The port that selenium is going to listen to | 4444 | no |
| CGB_URL | The cgb portal's url that you're going to test| http://cgb-frontend.sweatworks.tk/ | no |
| CGB_USER | The cgb username used to login into cgb portal |  | **yes** |
| CGB_PASSWORD | The cgb password used to login into cgb portal | - | **yes** |
| ORG_USER | The organizer username used to login into cgb portal  | - | **yes** |
| ORG_PASSWORD | The organizer password used to login into cgb portal | - | **yes** |

### Test Dependencies

First: `cptAddAccount.js`
Last: `cptDeactivateAcc.js`


